﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileWindowMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileOpenMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileSaveAsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FilePageSetupMenuSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.FilePageSetupMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FilePrintPreviewMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FilePrintMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FilePropertyMenuSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.FilePropertyMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewBackMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewForwardMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewHomeMenuSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.ViewHomeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewSearchMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewStopMenuSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ViewStopMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewRefreshMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewMenubarMenuSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.ViewMenubarMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewToolbarMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewStatusbarMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.オプションOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionPropertyMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ContextGoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextWindowMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContextBackMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextForwardMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.BackButton = New System.Windows.Forms.ToolStripButton()
        Me.ForwardButton = New System.Windows.Forms.ToolStripButton()
        Me.AddressTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me.GoButton = New System.Windows.Forms.ToolStripButton()
        Me.StopButton = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileMenuItem, Me.ViewMenuItem, Me.オプションOToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(386, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileMenuItem
        '
        Me.FileMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileWindowMenuItem, Me.FileOpenMenuItem, Me.FileSaveAsMenuItem, Me.FilePageSetupMenuSeparator, Me.FilePageSetupMenuItem, Me.FilePrintPreviewMenuItem, Me.FilePrintMenuItem, Me.FilePropertyMenuSeparator, Me.FilePropertyMenuItem, Me.FileExitMenuItem})
        Me.FileMenuItem.ImageTransparentColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.FileMenuItem.Name = "FileMenuItem"
        Me.FileMenuItem.RightToLeftAutoMirrorImage = True
        Me.FileMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.FileMenuItem.Text = "ファイル(&F)"
        '
        'FileWindowMenuItem
        '
        Me.FileWindowMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.NewWindow
        Me.FileWindowMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FileWindowMenuItem.Name = "FileWindowMenuItem"
        Me.FileWindowMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.FileWindowMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FileWindowMenuItem.Text = "新規ウィンドウ(&N)"
        '
        'FileOpenMenuItem
        '
        Me.FileOpenMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.OpenFolder2
        Me.FileOpenMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FileOpenMenuItem.Name = "FileOpenMenuItem"
        Me.FileOpenMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.FileOpenMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FileOpenMenuItem.Text = "ファイルを開く(&O)"
        '
        'FileSaveAsMenuItem
        '
        Me.FileSaveAsMenuItem.Name = "FileSaveAsMenuItem"
        Me.FileSaveAsMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FileSaveAsMenuItem.Text = "名前を付けて保存(&A)..."
        '
        'FilePageSetupMenuSeparator
        '
        Me.FilePageSetupMenuSeparator.Name = "FilePageSetupMenuSeparator"
        Me.FilePageSetupMenuSeparator.Size = New System.Drawing.Size(203, 6)
        '
        'FilePageSetupMenuItem
        '
        Me.FilePageSetupMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.PrintSetup
        Me.FilePageSetupMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FilePageSetupMenuItem.Name = "FilePageSetupMenuItem"
        Me.FilePageSetupMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FilePageSetupMenuItem.Text = "ページ設定(&U)"
        '
        'FilePrintPreviewMenuItem
        '
        Me.FilePrintPreviewMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.PrintPreview
        Me.FilePrintPreviewMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FilePrintPreviewMenuItem.Name = "FilePrintPreviewMenuItem"
        Me.FilePrintPreviewMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FilePrintPreviewMenuItem.Text = "印刷プレビュー(&V)"
        '
        'FilePrintMenuItem
        '
        Me.FilePrintMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Print
        Me.FilePrintMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FilePrintMenuItem.Name = "FilePrintMenuItem"
        Me.FilePrintMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.FilePrintMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FilePrintMenuItem.Text = "印刷(&P)"
        '
        'FilePropertyMenuSeparator
        '
        Me.FilePropertyMenuSeparator.Name = "FilePropertyMenuSeparator"
        Me.FilePropertyMenuSeparator.Size = New System.Drawing.Size(203, 6)
        '
        'FilePropertyMenuItem
        '
        Me.FilePropertyMenuItem.Name = "FilePropertyMenuItem"
        Me.FilePropertyMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FilePropertyMenuItem.Text = "プロパティ(&P)"
        '
        'FileExitMenuItem
        '
        Me.FileExitMenuItem.Name = "FileExitMenuItem"
        Me.FileExitMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.FileExitMenuItem.Text = "終了(&X)"
        '
        'ViewMenuItem
        '
        Me.ViewMenuItem.CheckOnClick = True
        Me.ViewMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewBackMenuItem, Me.ViewForwardMenuItem, Me.ViewHomeMenuSeparator, Me.ViewHomeMenuItem, Me.ViewSearchMenuItem, Me.ViewStopMenuSeparator2, Me.ViewStopMenuItem, Me.ViewRefreshMenuItem, Me.ViewMenubarMenuSeparator, Me.ViewMenubarMenuItem, Me.ViewToolbarMenuItem, Me.ViewStatusbarMenuItem})
        Me.ViewMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewMenuItem.Name = "ViewMenuItem"
        Me.ViewMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.ViewMenuItem.Text = "表示(&V)"
        '
        'ViewBackMenuItem
        '
        Me.ViewBackMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.NavBack
        Me.ViewBackMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewBackMenuItem.Name = "ViewBackMenuItem"
        Me.ViewBackMenuItem.ShortcutKeyDisplayString = "Alt+←"
        Me.ViewBackMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.Left), System.Windows.Forms.Keys)
        Me.ViewBackMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewBackMenuItem.Text = "前に戻る(&B)"
        '
        'ViewForwardMenuItem
        '
        Me.ViewForwardMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.NavForward
        Me.ViewForwardMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewForwardMenuItem.Name = "ViewForwardMenuItem"
        Me.ViewForwardMenuItem.ShortcutKeyDisplayString = "Alt+→"
        Me.ViewForwardMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.Right), System.Windows.Forms.Keys)
        Me.ViewForwardMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewForwardMenuItem.Tag = ""
        Me.ViewForwardMenuItem.Text = "次に進む(&F)"
        '
        'ViewHomeMenuSeparator
        '
        Me.ViewHomeMenuSeparator.Name = "ViewHomeMenuSeparator"
        Me.ViewHomeMenuSeparator.Size = New System.Drawing.Size(191, 6)
        '
        'ViewHomeMenuItem
        '
        Me.ViewHomeMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Home
        Me.ViewHomeMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewHomeMenuItem.Name = "ViewHomeMenuItem"
        Me.ViewHomeMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.ViewHomeMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewHomeMenuItem.Text = "ホームページ(&H)"
        '
        'ViewSearchMenuItem
        '
        Me.ViewSearchMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Search
        Me.ViewSearchMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewSearchMenuItem.Name = "ViewSearchMenuItem"
        Me.ViewSearchMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3
        Me.ViewSearchMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewSearchMenuItem.Text = "検索ページ(&S)"
        '
        'ViewStopMenuSeparator2
        '
        Me.ViewStopMenuSeparator2.Name = "ViewStopMenuSeparator2"
        Me.ViewStopMenuSeparator2.Size = New System.Drawing.Size(191, 6)
        '
        'ViewStopMenuItem
        '
        Me.ViewStopMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Critical
        Me.ViewStopMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewStopMenuItem.Name = "ViewStopMenuItem"
        Me.ViewStopMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.ViewStopMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewStopMenuItem.Text = "中止(&P)"
        '
        'ViewRefreshMenuItem
        '
        Me.ViewRefreshMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.RefreshDocView
        Me.ViewRefreshMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewRefreshMenuItem.Name = "ViewRefreshMenuItem"
        Me.ViewRefreshMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.ViewRefreshMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewRefreshMenuItem.Text = "最新情報に更新(&R)"
        '
        'ViewMenubarMenuSeparator
        '
        Me.ViewMenubarMenuSeparator.Name = "ViewMenubarMenuSeparator"
        Me.ViewMenubarMenuSeparator.Size = New System.Drawing.Size(191, 6)
        '
        'ViewMenubarMenuItem
        '
        Me.ViewMenubarMenuItem.Checked = True
        Me.ViewMenubarMenuItem.CheckOnClick = True
        Me.ViewMenubarMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ViewMenubarMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Control_MenuStrip
        Me.ViewMenubarMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewMenubarMenuItem.Name = "ViewMenubarMenuItem"
        Me.ViewMenubarMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewMenubarMenuItem.Text = "メニューバー(&M)"
        '
        'ViewToolbarMenuItem
        '
        Me.ViewToolbarMenuItem.Checked = True
        Me.ViewToolbarMenuItem.CheckOnClick = True
        Me.ViewToolbarMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ViewToolbarMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Control_ToolStrip
        Me.ViewToolbarMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewToolbarMenuItem.Name = "ViewToolbarMenuItem"
        Me.ViewToolbarMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewToolbarMenuItem.Text = "ツールバー(&T)"
        '
        'ViewStatusbarMenuItem
        '
        Me.ViewStatusbarMenuItem.Checked = True
        Me.ViewStatusbarMenuItem.CheckOnClick = True
        Me.ViewStatusbarMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ViewStatusbarMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Control_StatusStrip
        Me.ViewStatusbarMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ViewStatusbarMenuItem.Name = "ViewStatusbarMenuItem"
        Me.ViewStatusbarMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.ViewStatusbarMenuItem.Text = "ステータスバー(&A)"
        '
        'オプションOToolStripMenuItem
        '
        Me.オプションOToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OptionPropertyMenuItem})
        Me.オプションOToolStripMenuItem.Name = "オプションOToolStripMenuItem"
        Me.オプションOToolStripMenuItem.Size = New System.Drawing.Size(81, 20)
        Me.オプションOToolStripMenuItem.Text = "オプション(&O)"
        '
        'OptionPropertyMenuItem
        '
        Me.OptionPropertyMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.Properties
        Me.OptionPropertyMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.OptionPropertyMenuItem.Name = "OptionPropertyMenuItem"
        Me.OptionPropertyMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.OptionPropertyMenuItem.Text = "インターネットのプロパティ(&P)"
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.WebBrowser1)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(386, 273)
        Me.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStripContainer1.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.ToolStripContainer1.LeftToolStripPanelVisible = False
        Me.ToolStripContainer1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.RightToolStripPanelVisible = False
        Me.ToolStripContainer1.Size = New System.Drawing.Size(386, 302)
        Me.ToolStripContainer1.TabIndex = 1
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Controls.Add(Me.ToolStrip1)
        '
        'WebBrowser1
        '
        Me.WebBrowser1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser1.IsWebBrowserContextMenuEnabled = False
        Me.WebBrowser1.Location = New System.Drawing.Point(0, 0)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.ScriptErrorsSuppressed = True
        Me.WebBrowser1.Size = New System.Drawing.Size(386, 273)
        Me.WebBrowser1.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContextGoMenuItem, Me.ContextWindowMenuItem, Me.ToolStripSeparator1, Me.ContextBackMenuItem, Me.ContextForwardMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(180, 120)
        '
        'ContextGoMenuItem
        '
        Me.ContextGoMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.SearchWeb1
        Me.ContextGoMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ContextGoMenuItem.Name = "ContextGoMenuItem"
        Me.ContextGoMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ContextGoMenuItem.Text = "移動(&G)"
        '
        'ContextWindowMenuItem
        '
        Me.ContextWindowMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.NewWindow
        Me.ContextWindowMenuItem.Name = "ContextWindowMenuItem"
        Me.ContextWindowMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ContextWindowMenuItem.Text = "新規ウィンドウ(&N)"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(176, 6)
        '
        'ContextBackMenuItem
        '
        Me.ContextBackMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.NavBack
        Me.ContextBackMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ContextBackMenuItem.Name = "ContextBackMenuItem"
        Me.ContextBackMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ContextBackMenuItem.Text = "前に戻る(&B)"
        '
        'ContextForwardMenuItem
        '
        Me.ContextForwardMenuItem.Image = Global.WebBrowser3.My.Resources.Resources.NavForward
        Me.ContextForwardMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ContextForwardMenuItem.Name = "ContextForwardMenuItem"
        Me.ContextForwardMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ContextForwardMenuItem.Text = "次に進む(&F)"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.CanOverflow = False
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BackButton, Me.ForwardButton, Me.AddressTextBox, Me.GoButton, Me.StopButton})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(386, 29)
        Me.ToolStrip1.Stretch = True
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'BackButton
        '
        Me.BackButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BackButton.Image = Global.WebBrowser3.My.Resources.Resources.NavBack
        Me.BackButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BackButton.Name = "BackButton"
        Me.BackButton.Size = New System.Drawing.Size(23, 26)
        Me.BackButton.Text = "前に戻る"
        '
        'ForwardButton
        '
        Me.ForwardButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ForwardButton.Image = Global.WebBrowser3.My.Resources.Resources.NavForward
        Me.ForwardButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ForwardButton.Name = "ForwardButton"
        Me.ForwardButton.Size = New System.Drawing.Size(23, 26)
        Me.ForwardButton.Text = "次に進む"
        '
        'AddressTextBox
        '
        Me.AddressTextBox.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.AddressTextBox.Margin = New System.Windows.Forms.Padding(3)
        Me.AddressTextBox.Name = "AddressTextBox"
        Me.AddressTextBox.Size = New System.Drawing.Size(100, 23)
        Me.AddressTextBox.ToolTipText = "アドレス"
        '
        'GoButton
        '
        Me.GoButton.Image = Global.WebBrowser3.My.Resources.Resources.SearchWeb
        Me.GoButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GoButton.Name = "GoButton"
        Me.GoButton.Size = New System.Drawing.Size(51, 26)
        Me.GoButton.Text = "移動"
        '
        'StopButton
        '
        Me.StopButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.StopButton.Image = Global.WebBrowser3.My.Resources.Resources.Critical1
        Me.StopButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.StopButton.Name = "StopButton"
        Me.StopButton.Size = New System.Drawing.Size(23, 26)
        Me.StopButton.Text = "ToolStripButton1"
        Me.StopButton.ToolTipText = "中止"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripProgressBar1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 326)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(386, 23)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.AutoSize = False
        Me.ToolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("MS UI Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ToolStripStatusLabel1.Margin = New System.Windows.Forms.Padding(2, 3, 8, 2)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(272, 18)
        Me.ToolStripStatusLabel1.Spring = True
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripProgressBar1.Margin = New System.Windows.Forms.Padding(1, 3, 8, 3)
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(80, 17)
        Me.ToolStripProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(386, 349)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MainForm"
        Me.Text = " "
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ViewMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewBackMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewForwardMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewHomeMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewStopMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewRefreshMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents BackButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ForwardButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents AddressTextBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents GoButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents StopButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents FileMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ViewHomeMenuSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ViewStopMenuSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ViewSearchMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewToolbarMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewStatusbarMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewMenubarMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileOpenMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FilePageSetupMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FilePrintPreviewMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FilePrintMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FilePageSetupMenuSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FilePropertyMenuSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FilePropertyMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileSaveAsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents ViewMenubarMenuSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FileWindowMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents オプションOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionPropertyMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ContextGoMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextWindowMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ContextBackMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextForwardMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class

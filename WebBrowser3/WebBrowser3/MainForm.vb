﻿' Project:WebBrowser3
' Auther:IE3A No.20, 村田直人
' Date: 2016年05月10日

Imports System.IO

Public Class MainForm

    'アプリ名
    Private ReadOnly AppliTitle As String = My.Application.Info.Title

    'リンク先URL
    Dim LinkString As String

    'Form Closing(アプリ終了時の設定を保存)
    Private Sub MainForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing


        'アプリ設定の値を変更
        With My.Settings

            'menubarの設定値変更()
            .Menubar = ViewMenubarMenuItem.Checked

            'toolbarの設定値の変更
            .Toolbar = ViewToolbarMenuItem.Checked

            'statusbarの設定値の変更
            .Statusbar = ViewStatusbarMenuItem.Checked

        End With

    End Sub

    'Form Load'初期設定
    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        With Me

            .Text = AppliTitle 'タイトルの表示

            .Size = New Size(1000, My.Computer.Screen.Bounds.Height * 4 \ 5) '画面サイズ設定


        End With


        'アプリ設定
        With My.Settings

            'メニューバーの表示設定
            ViewMenubarMenuItem.Checked = .Menubar
            'ツールバー表示設定
            ViewToolbarMenuItem.Checked = .Toolbar
            'ステータスバー表示設定
            ViewStatusbarMenuItem.Checked = .Statusbar

        End With


        'Status Label,Progress 初期化
        ToolStripStatusLabel1.Text = ""
        ToolStripProgressBar1.Visible = False

        'Daialog Boxの初期設定
        With OpenFileDialog1

            'ファイルパス
            .FileName = ""

            'フィルタ設定
            .Filter = _
                "HTML ファイル (*.htm; *.html; *.mht; *.mhtml) " & _
                "|*.htm; *.html; *.mht; *.mhtml|" & _
                "XML ファイル(*.xml)|*.xml|" & _
                "テキストファイル(*.txt)|*.txt|" & _
                "画像ファイル(*.bmp; *.jpg; *.jpeg: *.bit; *.png)" & _
                "|*.bmp; *.jpg; *.jpeg: *.bit; *.png|" & _
                "PDFファイル(*.pdf)|*.pdf|" & _
                "すべてのファイル(*.*)|*.*"

        End With


        '現在のアプリケーション
        With My.Application

            'コマンドライン引数が1以上
            If .CommandLineArgs.Count > 0 Then


                WebBrowser1.Navigate(.CommandLineArgs(0))
            Else


                'ホームページに移動
                WebBrowser1.GoHome()
            End If
        End With

    End Sub

    '終了 Button
    Private Sub FileExitMenuItem_Click(sender As Object, e As EventArgs) Handles FileExitMenuItem.Click

        'Formを閉じる
        Me.Close()

    End Sub

    '「前に戻る」メニュー,「前に戻る」ボタン(コンテキスト)
    Private Sub ViewBackMenuItem_Click(sender As Object, e As EventArgs) Handles ViewBackMenuItem.Click, BackButton.Click, ContextBackMenuItem.Click


        '前のページに戻る
        WebBrowser1.GoBack()

    End Sub

    '「次に進む」メニュー,「次に進む」ボタン(コンテキスト)
    Private Sub ViewForwardMenuItem_Click(sender As Object, e As EventArgs) Handles ViewForwardMenuItem.Click, ForwardButton.Click, ContextForwardMenuItem.Click



        '次のページへ
        WebBrowser1.GoForward()

    End Sub

    'ホームページ
    Private Sub ViewHomeMenuItem_Click(sender As Object, e As EventArgs) Handles ViewHomeMenuItem.Click

        'ホームページへ
        WebBrowser1.GoHome()


    End Sub

    '中止
    Private Sub ViewStopMenuItem_Click(sender As Object, e As EventArgs) Handles ViewStopMenuItem.Click, StopButton.Click

        'キャンセル
        WebBrowser1.Stop()

    End Sub

    '更新 
    Private Sub ViewRefreshMenuItem_Click(sender As Object, e As EventArgs) Handles ViewRefreshMenuItem.Click

        'ブラウザ処理
        With WebBrowser1

            'URL未入力
            If .Url Is Nothing Then

                '処理終了
                Exit Sub
            End If

            '再読み込み
            .Refresh()

            '現在のページのURL = 入力された
            If .Url.ToString = AddressTextBox.Text Then

                '処理終了
                Exit Sub
            End If

            'アドレス表示 ?
            AddressTextBox.Text = .Url.ToString

        End With

        '移動ボタン　→　更新ボタン
        With GoButton

            .Text = "更新"
            .ToolTipText = "最新の情報に更新"
        End With


    End Sub

    '移動/更新
    Private Sub GoButton_Click(sender As Object, e As EventArgs) Handles GoButton.Click

        '「更新」時処理
        If GoButton.Text = "更新" Then

            '更新
            WebBrowser1.Refresh()
        End If

        '「移動」時処理
        Try

            'URL読み込み
            WebBrowser1.Navigate(AddressTextBox.Text)

        Catch ex As Exception

            MessageBox.Show(ex.Message, AppliTitle, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub

    'URL入力後 「Enter」
    Private Sub AddressTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles AddressTextBox.KeyDown


        '移動ボタン Clickイベント
        If e.KeyCode = Keys.Enter Then

            GoButton.PerformClick()
        End If
    End Sub

    'Address Text 変更時
    Private Sub AddressTextBox_TextChanged(sender As Object, e As EventArgs) Handles AddressTextBox.TextChanged

        'GoButton設定
        With GoButton

            'Text変更
            .Text = "移動"

            .ToolTipText = "" & AddressTextBox.Text & "へ移動"


        End With

    End Sub

    'ツールバー　サイズ変化時
    Private Sub ToolStrip1_SizeChanged(sender As Object, e As EventArgs) Handles ToolStrip1.SizeChanged

        'Text Box サイズ設定
        With AddressTextBox

            .Size = New Size(ToolStrip1.Size.Width - 144, .Size.Height)

        End With

    End Sub

    '戻るボタン有効/無効
    Private Sub WebBrowser1_CanGoBackChanged(sender As Object, e As EventArgs) Handles WebBrowser1.CanGoBackChanged, Me.Shown

        'WebBrowser設定
        With WebBrowser1
            '有効化・無効化
            ViewBackMenuItem.Enabled = .CanGoBack
            BackButton.Enabled = .CanGoBack

            'コンテキストメニュー(戻る)の「有効化/無効化」
            ContextBackMenuItem.Enabled = .CanGoBack
        End With

    End Sub

    '進むボタン有効/無効
    Private Sub WebBrowser1_CanGoForwardChanged(sender As Object, e As EventArgs) Handles WebBrowser1.CanGoForwardChanged, Me.Shown

        'WebBrowser設定
        With WebBrowser1

            '有効化・無効化
            ViewForwardMenuItem.Enabled = .CanGoForward
            ForwardButton.Enabled = .CanGoForward

            'コンテキストメニュー(進む)の「有効化/無効化」
            ContextForwardMenuItem.Enabled = .CanGoForward
        End With


    End Sub

    'ドキュメント読み込み終了時
    Private Sub WebBrowser1_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted

        'GoButton設定
        With GoButton

            '表示変更
            .Text = "更新"

            'ヒント表示
            .ToolTipText = "最新の情報に更新"

        End With

    End Sub

    'タイトル変化時
    Private Sub WebBrowser1_DocumentTitleChanged(sender As Object, e As EventArgs) Handles WebBrowser1.DocumentTitleChanged

        'ブラウザ
        With WebBrowser1

            'タイトル取得成功時
            If .DocumentTitle <> "" Then

                'フォームのタイトルにWebのタイトルを表示
                Me.Text = .DocumentTitle
            Else

                'URLからファイル名だけ取り出して表示
                Me.Text = Path.GetFileName(.Url.ToString())
            End If

        End With


        'Webタイトルとアプリ名を表示
        Me.Text &= " - " & AppliTitle

    End Sub

    'Navigatedイベント
    Private Sub WebBrowser1_Navigated(sender As Object, e As WebBrowserNavigatedEventArgs) Handles WebBrowser1.Navigated

        '表示中のURLを表示
        AddressTextBox.Text = WebBrowser1.Url.ToString

    End Sub

    '新ウィンドウ
    Private Sub WebBrowser1_NewWindow(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles WebBrowser1.NewWindow

        'イベントキャンセル
        e.Cancel = True
    End Sub

    'ダウンロード進行情報
    Private Sub WebBrowser1_ProgressChanged(sender As Object, e As WebBrowserProgressChangedEventArgs) Handles WebBrowser1.ProgressChanged

        'ダウンロー済みByte数、読み込む最大Byte数を格納した変数宣言
        Dim curtProg As Integer = Convert.ToInt32(e.CurrentProgress \ 1000)
        Dim maxProg As Integer = Convert.ToInt32(e.MaximumProgress \ 1000)

        'プログレスバー
        With ToolStripProgressBar1

            '非表示、終了判定
            If curtProg = 0 OrElse curtProg > maxProg Then

                .Visible = False '非表示
                Exit Sub         '終了
            End If

            'プログレスバー表示更新
            .Maximum = maxProg '最大値
            .Value = curtProg  '現在地

            .Visible = True    '表示
        End With

    End Sub

    'ステータス変更時
    Private Sub WebBrowser1_StatusTextChanged(sender As Object, e As EventArgs) Handles WebBrowser1.StatusTextChanged

        'ステータス表示
        ToolStripStatusLabel1.Text = WebBrowser1.StatusText()

    End Sub

    'メニューバー Menu Activate
    Private Sub MenuStrip1_MenuActivate(sender As Object, e As EventArgs) Handles MenuStrip1.MenuActivate

        'メニューバー表示
        MenuStrip1.Visible = True

    End Sub

    'メニューバー MenuDeactivate
    Private Sub MenuStrip1_MenuDeactivate(sender As Object, e As EventArgs) Handles MenuStrip1.MenuDeactivate

        'Menubarの表示・非表示設定
        MenuStrip1.Visible = ViewMenubarMenuItem.Checked

    End Sub


    '「ファイルを開く」メニュー
    Private Sub FileOpenMenuItem_Click(sender As Object, e As EventArgs) Handles FileOpenMenuItem.Click

        'ダイアログボックス
        With OpenFileDialog1

            'キャンセルボタンが押された時
            If .ShowDialog = DialogResult.Cancel Then

                '処理終了
                Exit Sub
            End If


            Try

                '選択したファイルを読み込む
                WebBrowser1.Navigate(.FileName)

            Catch ex As Exception

                'エラー文出力
                MessageBox.Show _
                    (ex.Message, AppliTitle, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End With

    End Sub

    '「名前を付けて保存」メニュー
    Private Sub FileSaveAsMenuItem_Click(sender As Object, e As EventArgs) Handles FileSaveAsMenuItem.Click


        '自動判定ダイアログボックスを開く
        WebBrowser1.ShowSaveAsDialog()

    End Sub

    '「ページ設定」メニュー
    Private Sub FilePageSetupMenuItem_Click(sender As Object, e As EventArgs) Handles FilePageSetupMenuItem.Click

        'ページ設定ダイアログを開く
        WebBrowser1.ShowPageSetupDialog()

    End Sub

    '「印刷プレビュー」メニュー
    Private Sub FilePrintPreviewMenuItem_Click(sender As Object, e As EventArgs) Handles FilePrintPreviewMenuItem.Click

        '印刷プレビューダイアログを開く
        WebBrowser1.ShowPrintPreviewDialog()

    End Sub

    '「プロパティ」メニュー
    Private Sub FilePrintMenuItem_Click(sender As Object, e As EventArgs) Handles FilePrintMenuItem.Click

        'プロパティダイアログを開く
        WebBrowser1.ShowPropertiesDialog()
    End Sub

    '「検索ページ」メニュー
    Private Sub ViewSearchMenuItem_Click(sender As Object, e As EventArgs) Handles ViewSearchMenuItem.Click

        '既定の検索ページに移動
        WebBrowser1.GoSearch()

    End Sub

    '「メニューバー」メニュー Click
    Private Sub ViewMenubarMenuItem_Click(sender As Object, e As EventArgs) Handles ViewMenubarMenuItem.Click, Me.Shown


        '表示・非表示切り替え
        MenuStrip1.Visible = ViewMenubarMenuItem.Checked
    End Sub

    '「ツールバー」メニュー Click
    Private Sub ViewToolbarMenuItem_Click(sender As Object, e As EventArgs) Handles ViewToolbarMenuItem.Click, Me.Shown

        '表示・非表示切り替え
        ToolStrip1.Visible = ViewToolbarMenuItem.Checked

    End Sub

    '「ステータスバー」メニュー Click
    Private Sub ViewStatusbarMenuItem_Click(sender As Object, e As EventArgs) Handles ViewStatusbarMenuItem.Click, Me.Shown

        '表示・非表示切り替え
        StatusStrip1.Visible = ViewStatusbarMenuItem.Checked

    End Sub

    '「新規ウィンドウ」メニュー
    Private Sub FileWindowMenuItem_Click(sender As Object, e As EventArgs) Handles FileWindowMenuItem.Click, _
        ContextWindowMenuItem.Click


        'Processクラスのインスタンス化
        Using proc As Process = New Process


            'Startメソッドに渡すプロパティ設定
            With proc.StartInfo

                '実行可能ファイル(.exe)の名前設定
                .FileName = Application.ExecutablePath

                'Clickしたオブジェクトが「新規ウィンドウ」で
                '               かつ()
                'リンク先URLが空文字列でないとき(文字数が0より上)
                If TryCast(sender, ToolStripMenuItem) Is ContextWindowMenuItem _
                    AndAlso LinkString.Length > 0 Then

                    '?
                    '.Arguments = """"c & LinkString & """"c
                    .Arguments = LinkString
                End If

                'エラーダイアログの表示
                .ErrorDialog = True
            End With

            Try

                'プロセス起動
                proc.Start()
            Catch ex As Exception

            End Try
        End Using
    End Sub

    '「インターネットのプロパティ」メニュー
    Private Sub OptionPropertyMenuItem_Click(sender As Object, e As EventArgs) Handles OptionPropertyMenuItem.Click

        'Processクラスのインスタンス作成
        Using proc As Process = New Process


            With proc.StartInfo


                '起動アプリに「インターネットオプション(inetcp1.cp1)」を指定
                .FileName = "inetcpl.cpl"

                'エラーダイアログの表示
                .ErrorDialog = True
            End With

            Try

                'プロセス起動
                proc.Start()
            Catch ex As Exception

            End Try
        End Using
    End Sub

    'コンテキストメニュー
    Private Sub ContextMenuStrip1_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip1.Opening

        Try

            'Uriオブジェクトのインスタンス化(URI変換)
            Dim linkUri As Uri = New Uri(WebBrowser1.StatusText)

            'リンク先URl変数にステータステキストを格納
            LinkString = WebBrowser1.StatusText

            'コンテキストメニューの「移動」を有効化
            ContextGoMenuItem.Enabled = True

        Catch ex As Exception

            'リンク先URL変数に空文字をセット
            LinkString = ""

            'コンテキストメニューの「移動」を無効化
            ContextGoMenuItem.Enabled = False
        End Try

    End Sub

    'コンテキスト「移動」メニュー
    Private Sub ContextGoMenuItem_Click(sender As Object, e As EventArgs) Handles ContextGoMenuItem.Click

        'URLの読み込み
        WebBrowser1.Navigate(LinkString)
    End Sub
End Class
